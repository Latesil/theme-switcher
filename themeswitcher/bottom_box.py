# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from .theme_switcher_constants import theme_switcher_constants as constants
import os
import datetime
from .helper_functions import set_wallpaper, set_theme, convert_to_values

@Gtk.Template(resource_path = constants["UI_PATH"] + 'ui/bottom_box.ui')
class BottomBox(Gtk.Box):

    __gtype_name__ = "BottomBox"

    _bottom_label = Gtk.Template.Child()
    _day_hour_spin_button = Gtk.Template.Child()
    _day_minutes_spin_button = Gtk.Template.Child()
    _night_hour_spin_button = Gtk.Template.Child()
    _night_minutes_spin_button = Gtk.Template.Child()
    _bottom_box_day_label = Gtk.Template.Child()
    _bottom_box_night_label = Gtk.Template.Child()
    _main_bottom_grid = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

        #init settings
        self.settings = Gio.Settings.new(constants["BASE_KEY"])
        self.theme_settings = Gio.Settings.new(constants["THEME_KEY"])
        self.wallpaper_settings = Gio.Settings.new(constants["WALLPAPER_KEY"])
        self.set_margin_top(20)

        self.current_light_theme = self.settings.get_string('light-theme')
        self.current_dark_theme = self.settings.get_string('dark-theme')
        
        self.night_wallpaper = self.settings.get_string('path-to-night-wallpaper')
        self.day_wallpaper = self.settings.get_string('path-to-day-wallpaper')

        #unused yet, but who knows
        # self._day_scale.set_name("day scale")
        # self._night_scale.set_name("night scale")

        #monitor changes in gsettings
        self.settings.connect("changed::daytime-hour", self.on__day_hour_spin_button_value_changed, self._day_hour_spin_button)
        self.settings.connect("changed::daytime-minutes", self.on__day_minutes_spin_button_value_changed, self._day_minutes_spin_button)
        self.settings.connect("changed::nighttime-hour", self.on__night_hour_spin_button_value_changed, self._night_hour_spin_button)
        self.settings.connect("changed::nighttime-minutes", self.on__night_minutes_spin_button_value_changed, self._night_hour_spin_button)
        self.settings.connect("changed::time-visible", self.on_time_visible_change, None)

        #get values from gsettings after start programm
        self.get_scales_values()
        self.get_values()
        self.on_combo_box_changed()

        self._bottom_box_day_label.set_halign(Gtk.Align.START)
        self._bottom_box_night_label.set_halign(Gtk.Align.START)


    #set active state for scales
    def on_time_visible_change(self, settings, key, button):
        if settings.get_boolean("time-visible"):
            self._main_bottom_grid.set_visible(True)
        else:
            self._main_bottom_grid.set_visible(False)

    def get_scales_values(self):
        self.on__day_hour_spin_button_value_changed(self.settings, "daytime-hour", self._day_hour_spin_button)
        self.on__day_minutes_spin_button_value_changed(self.settings, "daytime-minutes", self._day_minutes_spin_button)
        self.on__night_hour_spin_button_value_changed(self.settings, "nighttime-hour", self._night_hour_spin_button)
        self.on__night_minutes_spin_button_value_changed(self.settings, "nighttime-minutes", self._night_minutes_spin_button)
        self.on_time_visible_change(self.settings, None, None)
        
    def get_values(self):
        day_hour_values = self.settings.get_int("daytime-hour")
        day_minutes_values = self.settings.get_int("daytime-minutes")
        night_hour_values = self.settings.get_int("nighttime-hour")
        night_minutes_values = self.settings.get_int("nighttime-minutes")
        return day_hour_values, day_minutes_values, night_hour_values, night_minutes_values

    def on_combo_box_changed(self):
        current_time = datetime.datetime.now()
        values = self.get_values()
        
        #find better solution
        current_values = convert_to_values(current_time.hour, int(str(current_time.minute)[:-1]+'0'))
        
        day_values = convert_to_values(values[0], values[1])
        self.settings.set_int("daytime-values", day_values)
        
        night_values = convert_to_values(values[2], values[3])
        self.settings.set_int("nighttime-values", night_values)
        
        if ((current_values <= day_values or current_values >= night_values)):
            set_theme(self.theme_settings, self.current_dark_theme)
            if self.night_wallpaper is not None:
                set_wallpaper(self.wallpaper_settings, self.night_wallpaper)
        else:
            set_theme(self.theme_settings, self.current_light_theme)
            if self.day_wallpaper is not None:
                set_wallpaper(self.wallpaper_settings, self.day_wallpaper)
            
    #____functions for local changes from dconf____#
    
    def on__day_hour_spin_button_value_changed(self, settings, key, button):
        self._day_hour_spin_button.set_value(self.settings.get_int("daytime-hour"))
    
    def on__day_minutes_spin_button_value_changed(self, settings, key, button):
        self._day_minutes_spin_button.set_value(self.settings.get_int("daytime-minutes"))
    
    def on__night_minutes_spin_button_value_changed(self, settings, key, button):
        self._night_minutes_spin_button.set_value(self.settings.get_int("nighttime-minutes"))
    
    def on__night_hour_spin_button_value_changed(self, settings, key, button):
        self._night_hour_spin_button.set_value(self.settings.get_int("nighttime-hour"))
        
    #___adjustment callbacks____#
        
    @Gtk.Template.Callback()
    def on__day_hour_adjustment_value_changed(self, scale):
        self.settings.set_int("daytime-hour", scale.get_value())
        self.on_combo_box_changed()
        
    @Gtk.Template.Callback()
    def on__day_minutes_adjustment_value_changed(self, scale):
        self.settings.set_int("daytime-minutes", scale.get_value())
        self.on_combo_box_changed()
        
    @Gtk.Template.Callback()
    def on__night_minutes_adjustment_value_changed(self, scale):
        self.settings.set_int("nighttime-minutes", scale.get_value())
        self.on_combo_box_changed()
    
    @Gtk.Template.Callback()
    def on__night_hour_adjustment_value_changed(self, scale):
        self.settings.set_int("nighttime-hour", scale.get_value())
        self.on_combo_box_changed()
        
