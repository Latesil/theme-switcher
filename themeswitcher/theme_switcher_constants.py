# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

theme_switcher_constants = {
    "APP_ID":"com.github.Latesil.theme-switcher",
    "APP_NAME": "Theme Switcher",
    "BASE_KEY":"com.github.Latesil.theme-switcher",
    "WALLPAPER_KEY":"org.gnome.desktop.background",
    "THEME_KEY":"org.gnome.desktop.interface",
    "UI_PATH":'/com/github/Latesil/theme-switcher/',
    "default_light_theme": "Adwaita",
    "default_dark_theme": "Adwaita-dark"
}
